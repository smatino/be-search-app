var express = require('express');
var cors = require('cors');
var axios = require('axios');
var redis = require('redis');

let client;

(async () => {
  client = redis.createClient({
    url: 'redis://red-ch9mdo2k728hts2um1kg:6379'
    //url: 'rediss://red-ch9mdo2k728hts2um1kg:WHR5MjXSa0kfA76iU6PG8k6gVtMx34gw@frankfurt-redis.render.com:6379'
});

    client.on("error", (error) => console.error(`Error : ${error}`));

  await client.connect();
  
})();

var app = express();
app.use(cors());


app.get('/api/search', function (req, res) {
  
  console.log(req.query);
  var per_page = req.query.per_page;
  var page = req.query.page;
  var city = req.query.city;
  var country = req.query.country;
  var url = 'https://api.partoo.co/v2/business/search';
  if(city){
    url = url + '?city='+city;
    if(country){
      url = url + '&country='+country;
    }
    if(per_page){
      url = url + '&per_page='+per_page;
    }  
  }
  else if(country){
    url = url + '?country='+country;
    if(per_page){
      url = url + '&per_page='+per_page;
    } 
  }else if(per_page){
    url = url + '?per_page='+per_page+'&page='+page;
  } 
  
  try{

    (async () => {
   
      console.log('url->'+url);
      const value = await client.get(url);

      if (value) {
        console.log('Users retrieved from Redis');
        res.status(200).send(JSON.parse(value));
      } else {
        axios.get(url,{
          headers: {
          'x-APIKey': '324e538ac5f187167ad512776abcacc4cd668e243d852706'
          }
        })
        .then(response => {
            client.set(url, JSON.stringify(response.data));
            res.send(response.data);
            console.log('Users retrieved from the API');
        })
      }
    })();
  }
  catch (err) {
    res.status(500).send({ error: err.message });
  }
  
});


app.get('/api/location/:address', function (req, res) {
  let address = req.params.address;
  axios.get('https://api.mapbox.com/geocoding/v5/mapbox.places/'+address+'.json?access_token=pk.eyJ1IjoicmljY2FyZG90ZXN0IiwiYSI6ImNsZzNyems5cjA4OGIzbnFlM2VmNDFpN2IifQ.ruQVegJEwzizX4or7CrdYg')
  .then(response => {
    res.send(response.data);
  })
  .catch(err => {
    console.log('Error: ', err.message);
  });
});

app.listen(3000, function () {
  console.log('app listening on port 3000!');
  
});